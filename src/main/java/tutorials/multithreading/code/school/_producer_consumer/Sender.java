package tutorials.multithreading.code.school._producer_consumer;

import java.util.Random;

public class Sender implements Runnable {

    private final Data data;
    String[] packets = {"First packet \n", "Second packet \n", "Third packet \n", "Fourth packet \n", "End"};


    public Sender(Data data) {
        this.data = data;
    }

    @Override
    public void run() {
        while (true) {
            try {
                for (int i = 0; i < packets.length; i++) {
                    data.put(packets[i]);
                    Random random = new Random();
                    int sleepTime =  random.nextInt(5000);
                    Thread.sleep(sleepTime);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}





