package tutorials.multithreading.code.school._producer_consumer;

public class Receiver implements Runnable {

    private final Data data;

    public Receiver(Data data){
        this.data = data;
    }

    @Override
    public void run(){
        while (true){
            try{
                String element = data.get();
                if(element.equals("End")){
                    System.exit(1);
                }else
                    System.out.println(element);
            }catch (InterruptedException e){
                e.printStackTrace();
                return;
            }
        }
    }
}
