package tutorials.multithreading.code.school._producer_consumer;

public class Data {
    private String packet = null;
    // private boolean transfer;

    //receive()
    public synchronized void put(String newPacket) throws InterruptedException{
        while (packet!=null){
            this.wait();
        }
        this.packet = newPacket;
        this.notifyAll();
    }

    //send()
    public synchronized String get() throws InterruptedException{
        while (packet == null){
            this.wait();
        }
        String result = this.packet;
        this.packet = null;
        this.notifyAll();
        return result;
    }

}
