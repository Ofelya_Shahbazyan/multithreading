package tutorials.multithreading.code.school._producer_consumer;

import java.util.Random;

public class ThreadDemo {
    public static void main(String[] args) {
        Data data = new Data();

        new Thread(new Sender(data)).start();
        new Thread(new Receiver(data)).start();
    }
}

