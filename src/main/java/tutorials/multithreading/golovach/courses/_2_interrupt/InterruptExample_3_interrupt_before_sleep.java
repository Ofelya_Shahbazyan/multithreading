package tutorials.multithreading.golovach.courses._2_interrupt;

public class InterruptExample_3_interrupt_before_sleep {
    public static void main(String[] args) {
        Thread.currentThread().interrupt();
        try{
//            System.out.println(Thread.interrupted());
            Thread.sleep(Long.MAX_VALUE);
        }catch(InterruptedException e){
            System.out.println("Interrupted by exception.");
        }
    }
}
