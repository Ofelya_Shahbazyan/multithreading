package tutorials.multithreading.golovach.courses._2_interrupt;

/**
 * Демонстрация работы статического метода Thread.interrupted() - читает значение флага прерывания, очищая его.
 * Т.е. первое же чтение очищает флаг
 */
public class InterruptExample_1_interrupted {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println(Thread.interrupted());
                    for (long k = 0; k < 1_000_000_000L; k++) ;
                }
            }
        });
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
    }
}
