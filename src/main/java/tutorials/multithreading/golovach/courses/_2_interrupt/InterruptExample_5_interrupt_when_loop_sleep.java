package tutorials.multithreading.golovach.courses._2_interrupt;

/**
 * Демонстрация передачи сигнала о прерывании одного потока - другому,
 * при этом не только выставляется флаг прерывания (доступный для чтения по isInterrupted()), но и
 * 1) попытка войти в метод sleep() с выставленным флагом
 * 2) или изменение состояния флага когда метод спит (sleep())
 * приводит к выбрасыванию InterruptedException
 */
public class InterruptExample_5_interrupt_when_loop_sleep {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Thread myThread = Thread.currentThread();
                while (!myThread.isInterrupted()) {
                    System.out.println("Hello!");
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        System.out.println("I interrupted: by exception");
                        return;
                    }
                }
                System.out.println("I interrupted: by flag");
            }
        });
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
    }
}
