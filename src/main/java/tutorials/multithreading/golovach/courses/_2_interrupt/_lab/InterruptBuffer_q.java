//package tutorials.multithreading.golovach.courses._2_interrupt._lab;
//
///**
// *  Предлагается сделать блокирующий буфер на один элемент,
// *  но передачу сообщения делать не на основе wait()/notify(),
// *  а на основе wait()/interrupt(). В следующей ниже "заготовке" замените '// ?' на свои строки.
// *  Основная идея заключается в том, что мы явно поддерживаем односвязные списки ожидающих производителей и потребителей.
// *  Если нет работы, то поток сам себя добавляет в соответствующий список. Если поток создал работу,
// *  то он удаляет поток из соответствующего списка и "будит" его.
// *
// *     "Заготовка" буфера:
// */
//public class InterruptBuffer_q {
//    private ThreadNode producers = null;
//    private ThreadNode consumers = null;
//    private Integer elem = null;
//
//    public synchronized void put(int newElem) {
//        while (elem != null) {
//            try {
//                // ?
//                this.wait();
//            } catch (InterruptedException e) {/*NOP*/}
//        }
//        elem = newElem;
//        if (consumers != null) {
//            consumers.thread.interrupt();
//            // ?
//        }
//    }
//
//    public synchronized int get() {
//        while (elem == null) {
//            try {
//                // ?
//                this.wait();
//            } catch (InterruptedException e) {/*NOP*/}
//        }
//        int result = elem;
//        elem = null;
//        if (producers != null) {
//            producers.thread.interrupt();
//            // ?
//        }
//        return result;
//    }
//}
