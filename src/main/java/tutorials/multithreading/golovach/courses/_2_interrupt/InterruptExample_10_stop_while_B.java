package tutorials.multithreading.golovach.courses._2_interrupt;

public class InterruptExample_10_stop_while_B {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
//                    while (true) {
//                        System.out.println("Hello!");
                        for (long k = 0; k < 1_000_000_000L; k++) ;
//                    }
                } catch (ThreadDeath e) {
                    System.out.println(e);
                }
            }
        });
        thread.start();
        Thread.sleep(1000);
        thread.stop();
        Thread.sleep(1000);
        thread.stop();

        Thread.sleep(1000);
        thread.stop();

        Thread.sleep(1000);
        thread.stop();

        Thread.sleep(1000);
        thread.stop();

        Thread.sleep(1000);
        thread.stop();

    }
}
