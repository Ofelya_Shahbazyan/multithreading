package tutorials.multithreading.golovach.courses._2_interrupt;

/**
 *  wait() ведет себя аналогично sleep()
 */
public class InterruptExample_6_interrupt_when_loop_wait {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Object lock = new Object();
                Thread myThread = Thread.currentThread();
                while (!myThread.isInterrupted()) {
                    System.out.println("Hello!");
                    try {
                        synchronized (lock) {
                            lock.wait(200);
                        }
                    } catch (InterruptedException e) {
                        System.out.println("I interrupted: by exception");
                        return;
                    }
                }
                System.out.println("I interrupted: by flag");
            }
        });
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
    }
}
