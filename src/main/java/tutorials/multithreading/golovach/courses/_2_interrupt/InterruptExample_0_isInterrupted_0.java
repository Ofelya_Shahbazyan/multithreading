package tutorials.multithreading.golovach.courses._2_interrupt;

public class InterruptExample_0_isInterrupted_0 {
        public static void main(String[] args) throws InterruptedException {
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    Thread myThread = Thread.currentThread();
                    while (true) {
                        System.out.println(myThread.isInterrupted());
                        for (long k = 0; k < 1_000_000_000L; k++) ;
                    }
                }
            };

            System.out.println(run.getClass());

            Thread thread = new Thread(run);
            thread.start();
            Thread.sleep(1000);
            thread.interrupt();
        }
    }
