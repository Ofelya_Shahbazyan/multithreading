package tutorials.multithreading.golovach.courses._2_interrupt;

public class InterruptExample_8_interrupt_when_wait {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("I will sleep.");
                    Object lock = new Object();
                    synchronized (lock) {
                        lock.wait(Long.MAX_VALUE);
                    }
                } catch (InterruptedException e) {
                    System.out.println("I interrupted by exception");
                }
            }
        });
        thread.start();
        Thread.sleep(3000);
        System.out.println("Wake up!");
        thread.interrupt();
    }
}
