package tutorials.multithreading.golovach.courses._1_sync_wait_notify._lab.timed_wait;

/**
 * Задание: Вам дана "заготовка" класса SingleElementBufferTimed,
 * который должен в случае превышения времени ожидания в wait(long) выбросить исключение TimeoutException.
 * В данный момент он ожидает корректно, но исключение не выбрасывает.
 * Ваша задача дописать что-то в местах комментариев, что решает бросать исключение или нет и, в случае надобности, бросает:
 */
import java.util.concurrent.TimeoutException;

public class SingleElementBufferTimed_question {
    private Integer elem = null;

    public synchronized void put(Integer newElem, long timeout) throws InterruptedException, TimeoutException {
        long waitTime = timeout;
        while (elem != null && waitTime > 0) {
            long t0 = System.currentTimeMillis();
            wait(waitTime);
            long t1 = System.currentTimeMillis();
            long elapsedTime = t1 - t0;
            waitTime -= elapsedTime;
        }
        // todo: insert throw new TimeoutException
        this.elem = newElem;
        this.notifyAll();
    }

    public synchronized Integer get(long timeout) throws InterruptedException, TimeoutException {
        long waitTime = timeout;
        while (elem == null && waitTime > 0) {
            long t0 = System.currentTimeMillis();
            wait(waitTime);
            long t1 = System.currentTimeMillis();
            long elapsedTime = t1 - t0;
            waitTime -= elapsedTime;
        }
        // todo: insert throw new TimeoutException
        int result = this.elem;
        this.elem = null;
        this.notifyAll();
        return result;
    }
}
