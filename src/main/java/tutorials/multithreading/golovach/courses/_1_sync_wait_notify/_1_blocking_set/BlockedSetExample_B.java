package tutorials.multithreading.golovach.courses._1_sync_wait_notify._1_blocking_set;

/**
 *  Стартуем одновременно 5 потоков и заставляем из одновременно вызвать синхронизированный метод f() одного объекта.
 *  Отметьте временную динамику - сразу выводится число со знаком "+",
 *  через секунду - пара (число со знаком "-" и число со знаком "+")
 *  (порядок вывода чисел - не детерминирован = за "+x" всегда идет "-x", но за "-x" не определено какой именно будет "+y"):
 * x
 */
public class BlockedSetExample_B {

    public static void main(String[] args) throws InterruptedException {
        BlockedSetExample_B ref = new BlockedSetExample_B();
        for (int k = 0; k < 5; k++) {
            new Thread(new BlockedMethodCaller_B(ref, k)).start();
        }
    }

    public synchronized void f(int x) throws InterruptedException {
        System.out.print(" +" + x);
        Thread.sleep(1000);
        System.out.print(" -" + x);
    }
}
