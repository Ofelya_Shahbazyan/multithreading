package tutorials.multithreading.golovach.courses._1_sync_wait_notify._2_wait_set;

/**
 *  Стартуем одновременно 5 потоков и заставляем из одновременно вызвать синхронизированный метод f() одного объекта. Отметьте временную динамику - сразу выводится 5 чисел со знаком "+", через секунду - 5 чисел со знаком "-" (порядок вывода чисел - не детерминирован):
 * x
 */
public class WaitSetExample {

    public static void main(String[] args) throws InterruptedException {
        WaitSetExample ref = new WaitSetExample();
        for (int k = 0; k < 5; k++) {
            new Thread(new WaitMethodCaller(ref, k)).start();
        }
    }

    public synchronized void f(int x) throws InterruptedException {
        System.out.println("+" + x);
        this.wait();
        System.out.println("-" + x);
    }
}
