package tutorials.multithreading.golovach.courses._1_sync_wait_notify._1_blocking_set;

public class BlockedMethodCaller_C implements Runnable {
    private final BlockedSetExample_C ref;
    private final int k;

    public BlockedMethodCaller_C(BlockedSetExample_C ref, int k) {
        this.ref = ref;
        this.k = k;
    }

    @Override
    public void run() {
        try {
            ref.f(k);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
