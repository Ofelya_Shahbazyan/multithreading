package tutorials.multithreading.golovach.courses._1_sync_wait_notify._1_blocking_set;

/**
 *   Стартуем одновременно 5 потоков и заставляем из одновременно вызвать синхронизированный метод f() разных объектов одного класса. Отметьте временную динамику - сразу выводится 5 чисел со знаком "+", через секунду - 5 чисел со знаком "-" (порядок вывода чисел - не детерминирован):
 * x
 */
public class BlockedSetExample_C {

    public static void main(String[] args) throws InterruptedException {
        for (int k = 0; k < 5; k++) {
            new Thread(new BlockedMethodCaller_C(new BlockedSetExample_C(), k)).start();
        }
    }

    public synchronized void f(int x) throws InterruptedException {
        System.out.println("+" + x);
        Thread.sleep(1000);
        System.out.println("-" + x);
    }
}
