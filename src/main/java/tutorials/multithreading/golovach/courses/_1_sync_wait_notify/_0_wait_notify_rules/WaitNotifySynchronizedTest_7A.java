package tutorials.multithreading.golovach.courses._1_sync_wait_notify._0_wait_notify_rules;

public class WaitNotifySynchronizedTest_7A {
    public static void main(String[] args) {
        f();
    }
    public static synchronized void f() {
        Class clazz = WaitNotifySynchronizedTest_7A.class;
        clazz.notify();
    }
}
