package tutorials.multithreading.golovach.courses._1_sync_wait_notify._1_blocking_set;

/**
 *  Вспомогательный класс (в методе run() вызовет метод f() объекта, полученного в конструкторе): x
 */
public class BlockedMethodCaller_A implements Runnable {
    private final BlockedSetExample_A ref;
    private final int k;

    public BlockedMethodCaller_A(BlockedSetExample_A ref, int k) {
        this.ref = ref;
        this.k = k;
    }

    @Override
    public void run() {
        try {
            ref.f(k);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
