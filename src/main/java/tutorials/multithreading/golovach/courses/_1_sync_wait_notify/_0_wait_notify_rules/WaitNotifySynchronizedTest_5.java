package tutorials.multithreading.golovach.courses._1_sync_wait_notify._0_wait_notify_rules;

public class WaitNotifySynchronizedTest_5 {
    public static void main(String[] args) {
        Object ref0 = new Object();
        Object ref1 = new Object();
        synchronized (ref0) {
            synchronized (ref0) {
                ref0.notify();
            }
        }
    }
}
