package tutorials.multithreading.golovach.courses._1_sync_wait_notify._0_wait_notify_rules;

public class WaitNotifySynchronizedTest_6A {
    public static void main(String[] args) {
        new WaitNotifySynchronizedTest_6A().f();
    }
    public synchronized void f() {
        this.notify();
    }
}
