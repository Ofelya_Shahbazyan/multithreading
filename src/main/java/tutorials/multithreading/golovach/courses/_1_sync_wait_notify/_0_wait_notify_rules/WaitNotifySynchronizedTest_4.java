package tutorials.multithreading.golovach.courses._1_sync_wait_notify._0_wait_notify_rules;

public class WaitNotifySynchronizedTest_4 {
    public static void main(String[] args){
        synchronized (new Object()) {
            new Object().notify();
        }
    }
}
