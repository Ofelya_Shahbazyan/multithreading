package tutorials.multithreading.golovach.courses._1_sync_wait_notify._0_wait_notify_rules;

public class WaitNotifySynchronizedTest_1_1 {
        public static void main(String[] args) {
            Object ref = new Object();
            synchronized (ref) {
                ref.notifyAll();
            }
        }
    }
