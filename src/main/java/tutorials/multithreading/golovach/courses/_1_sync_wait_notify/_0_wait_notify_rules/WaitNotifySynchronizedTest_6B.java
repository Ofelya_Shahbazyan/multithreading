package tutorials.multithreading.golovach.courses._1_sync_wait_notify._0_wait_notify_rules;

public class WaitNotifySynchronizedTest_6B {
    public static void main(String[] args) throws InterruptedException {
        new WaitNotifySynchronizedTest_6B().f();
    }
    public void f() {
        synchronized (this) {
            this.notify();
        }
    }
}
