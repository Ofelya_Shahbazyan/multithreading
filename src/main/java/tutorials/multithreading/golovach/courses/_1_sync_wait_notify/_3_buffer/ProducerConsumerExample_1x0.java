package tutorials.multithreading.golovach.courses._1_sync_wait_notify._3_buffer;

public class ProducerConsumerExample_1x0 {
    public static void main(String[] args) {
        SingleElementBuffer buffer = new SingleElementBuffer();
        new Thread(new Producer(1, 1000, buffer)).start();
    }
}
