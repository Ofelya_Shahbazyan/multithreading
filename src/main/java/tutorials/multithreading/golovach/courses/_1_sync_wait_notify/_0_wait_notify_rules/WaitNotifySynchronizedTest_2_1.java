package tutorials.multithreading.golovach.courses._1_sync_wait_notify._0_wait_notify_rules;

public class WaitNotifySynchronizedTest_2_1 {
        public static void main(String[] args) throws InterruptedException {
//            Object ref = new Object();
            Object ref = null;
            synchronized (ref) {
                ref.wait();
            }
        }
    }
