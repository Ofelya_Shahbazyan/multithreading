package tutorials.multithreading.golovach.courses._1_sync_wait_notify._1_blocking_set;

/**
 * Стартуем одновременно 5 потоков и заставляем из одновременно вызвать НЕ синхронизированный метод f() одного объекта.
 * Отметьте временную динамику - сразу выводится 5 чисел со знаком "+", через секунду - 5 чисел со знаком "-"
 * (порядок вывода чисел - не детерминирован): x
 */
public class BlockedSetExample_A {

    public static void main(String[] args) throws InterruptedException {
        BlockedSetExample_A ref = new BlockedSetExample_A();
        for (int k = 0; k < 5; k++) {
            new Thread(new BlockedMethodCaller_A(ref, k)).start();
        }
    }

    public void f(int x) throws InterruptedException {
        System.out.print(" +" + x);
        Thread.sleep(1000);
        System.out.print(" -" + x);
    }
}
