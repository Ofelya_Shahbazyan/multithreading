//package tutorials.multithreading.golovach.courses._1_sync_wait_notify._lab.timed_wait;
//
//import tutorials.multithreading.golovach.courses._1_sync_wait_notify._lab.timed_wait.ConsumerTimed;
//
///**
// *  С ним одинокий Потребитель не ждет вечно
// */
//public class SingleConsumerTimedExample {
//    public static void main(String[] args) {
//        SingleElementBufferTimed buffer = new SingleElementBufferTimed();
//        new Thread(new ConsumerTimed(buffer, 3000), "Consumer").start();
//    }
//}
