//package tutorials.multithreading.golovach.courses._1_sync_wait_notify._lab.timed_wait;
//
//public class FiveSlowProducersExample_B {
//    public static void main(String[] args) throws InterruptedException {
//        int producerSleepTime=1200;
//        int consumerWaitTime=1000;
//
//        //buffer
//        SingleElementBufferTimed buffer =new SingleElementBufferTimed();
//        //consumer
//        new Thread(new ConsumerTimed(buffer, consumerWaitTime), "Consumer").start();
//        //producer
//        new Thread(new ProducerTimed(1, producerSleepTime, buffer, 100), "Producer-100");
//        Thread.sleep(400);
//        new Thread(new ProducerTimed(100, producerSleepTime, buffer, 100), "Producer-200");
//        Thread.sleep(400);
//        new Thread(new ProducerTimed(10000, producerSleepTime, buffer, 100), "Producer-300");
//    }
//}
