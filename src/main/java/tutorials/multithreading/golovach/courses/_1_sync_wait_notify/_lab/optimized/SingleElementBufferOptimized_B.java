package tutorials.multithreading.golovach.courses._1_sync_wait_notify._lab.optimized;

/**
 * В качестве ответа на лабораторную напишите - будет ли корректно или некорректно
 * работать эта реализация ограниченного блокирующего буфера и почему
 */
public class SingleElementBufferOptimized_B {
    private Integer elem = null;

    public synchronized void put(Integer newElem) throws InterruptedException {
        while (elem != null) {
            this.wait();
            if (elem != null) {
                this.notify();
            }
        }
        this.elem = newElem;
        this.notify();
    }

    public synchronized Integer get() throws InterruptedException {
        while (elem == null) {
            this.wait();
            if (elem == null) {
                this.notify();
            }
        }
        int result = this.elem;
        this.elem = null;
        this.notify();
        return result;
    }
}
