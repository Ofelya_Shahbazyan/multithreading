package tutorials.multithreading.golovach.courses._1_sync_wait_notify._lab.optimized;

import tutorials.multithreading.golovach.courses._1_sync_wait_notify._3_buffer.Consumer;
import tutorials.multithreading.golovach.courses._1_sync_wait_notify._3_buffer.SingleElementBuffer;

/**
 * Аналогично, если нет производителей, но есть потребители,
 * то потребители блокируются навечно (в реализации буфера SingleElementBuffer с лекции):
 */
public class Test2 {
        public static void main(String[] args) {
            SingleElementBuffer buffer = new SingleElementBuffer();
            new Thread(new Consumer(buffer)).start();
        }
    }
