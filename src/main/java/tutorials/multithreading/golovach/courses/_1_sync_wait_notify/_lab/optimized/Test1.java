package tutorials.multithreading.golovach.courses._1_sync_wait_notify._lab.optimized;

import tutorials.multithreading.golovach.courses._1_sync_wait_notify._3_buffer.Producer;
import tutorials.multithreading.golovach.courses._1_sync_wait_notify._3_buffer.SingleElementBuffer;

/**
 *  Заметим, что если нет потребителей, но есть производители,
 *  то производители блокируются навечно (в реализации буфера SingleElementBuffer с лекции):
 */
public class Test1 {
        public static void main(String[] args) {
            SingleElementBuffer buffer = new SingleElementBuffer();
            new Thread(new Producer(1, 1000, buffer)).start();
        }
    }
