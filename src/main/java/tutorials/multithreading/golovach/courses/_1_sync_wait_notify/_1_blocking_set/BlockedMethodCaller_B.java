package tutorials.multithreading.golovach.courses._1_sync_wait_notify._1_blocking_set;

public class BlockedMethodCaller_B implements Runnable {
    private final BlockedSetExample_B ref;
    private final int k;

    public BlockedMethodCaller_B(BlockedSetExample_B ref, int k) {
        this.ref = ref;
        this.k = k;
    }

    @Override
    public void run() {
        try {
            ref.f(k);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
