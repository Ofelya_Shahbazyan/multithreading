package tutorials.multithreading.golovach.courses._0_thread_runnable;

public class Lab_Thread_Thread_Play_The_According_Bad {
    public static void main(String[] args) throws InterruptedException {
        for (int k = 0; k < 10; k++) {
            //A+B
            Runnable printerA = new PrintRunnable("A  .", 100);
            Thread threadA = new Thread(printerA);
            threadA.start();
//            Runnable printerB = new PrintRunnable(".  B", 100);
            Runnable printerB = new PrintRunnable(".  B", 99);
            Thread threadB = new Thread(printerB);
            threadB.start();
//            System.out.println(System.currentTimeMillis());
//            System.out.println(System.nanoTime());
            threadA.join();
//            System.out.println(System.currentTimeMillis());
//            System.out.println(System.nanoTime());
            threadB.join();
            //C
            System.out.println("-----");
            Runnable printerC = new PrintRunnable("  C", 100);
            printerC.run();
            System.out.println("-----");
        }
    }
}
