package tutorials.multithreading.golovach.courses._0_thread_runnable;

public class ThreadExample_3A_0 {
    public static void main(String[] args) throws InterruptedException {
//        Thread thread = Thread.currentThread();
//        thread.join();
        Thread.currentThread().join();
    }
}
