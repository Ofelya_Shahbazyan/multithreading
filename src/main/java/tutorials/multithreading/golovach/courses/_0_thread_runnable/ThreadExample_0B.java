package tutorials.multithreading.golovach.courses._0_thread_runnable;

public class ThreadExample_0B {
    public static void main(String[] args) throws InterruptedException {
        for (int k = 0; k < 10; k++) {
            Thread.sleep(240);
            System.out.println("A");
            f();
        }
    }

    public static void f() throws InterruptedException {
        Thread.sleep(260);
        System.out.println(" B");
    }
}
