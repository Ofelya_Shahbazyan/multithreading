package tutorials.multithreading.golovach.courses._0_thread_runnable;

public class ThreadExample_2A {
    public static void main(String[] args) throws InterruptedException {

        Runnable printer = new PrintRunnable(" B", 100);
        Thread thread = new Thread(printer);
        thread.start();

        System.out.println("AAAA");
        thread.join();
        System.out.println("BBBB");

        for (int k = 0; k < 10; k++) {
            Thread.sleep(250);
            System.out.println("A");
        }

    }
}

