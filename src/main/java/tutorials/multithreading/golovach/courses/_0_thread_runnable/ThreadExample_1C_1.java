package tutorials.multithreading.golovach.courses._0_thread_runnable;

public class ThreadExample_1C_1 {
    public static void main(String[] args) throws InterruptedException {
        for (int k = 0; k < 10; k++) {
            Thread.sleep(250);
            System.out.println("A");
        }

        Runnable printer = new PrintRunnable(" B", 1000);
        Thread thread = new Thread(printer);
        thread.start();

        System.out.println("FINISH");


    }
}
