package tutorials.multithreading.journaldev.com._1_sync_wait_notify._0_producer_consumer_problem;

public class Message {
    private String msg;

    public Message(String str){
        this.msg=str;
    }

    public String getMsg() {
        return msg;
    }

}
