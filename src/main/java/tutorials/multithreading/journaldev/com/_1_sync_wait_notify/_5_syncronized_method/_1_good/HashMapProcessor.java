package tutorials.multithreading.journaldev.com._1_sync_wait_notify._5_syncronized_method._1_good;

class HashMapProcessor implements Runnable{

    private String[] strArr = null;
    private Object lock = new Object();

    public HashMapProcessor(String[] m){
        this.strArr=m;
    }

    public String[] getMap() {
        return strArr;
    }

    @Override
    public void run() {
        processArr(Thread.currentThread().getName());
    }

    private void processArr(String name) {
        for(int i=0; i < strArr.length; i++){
            //process data and append thread name
            processSomething(i);
            addThreadName(i, name);
        }
    }

    /**
     * The String array values are corrupted because of shared data and no synchronization.
     * Here is how we can change addThreadName() method to make our program thread-safe.
     * @param i
     * @param name
     */
    private void addThreadName(int i, String name) {
        synchronized(lock){
            strArr[i] = strArr[i] +":"+name;
        }
    }

    private void processSomething(int index) {
        // processing some job
        try {
            Thread.sleep(index*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
