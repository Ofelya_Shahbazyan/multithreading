package tutorials.multithreading.journaldev.com._1_sync_wait_notify._4_syncronized_method_bad;

/**
 * Notice that lock Object is public and by changing its reference,
 * we can execute synchronized block parallel in multiple threads.
 * A similar case is true if you have private Object but have a setter method to change its reference.
 */
public class MyObject_B {
        public Object lock = new Object();

        public void doSomething() {
            synchronized (lock) {
                // ...
            }
        }
    }

//untrusted code

//    MyObject_B myObject = new MyObject_B();
////change the lock Object reference
//    myObject.lock = new Object();
