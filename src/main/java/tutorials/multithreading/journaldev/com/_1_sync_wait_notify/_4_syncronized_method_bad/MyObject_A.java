package tutorials.multithreading.journaldev.com._1_sync_wait_notify._4_syncronized_method_bad;


/**
 * Notice that hacker’s code is trying to lock the myObject instance and once it gets the lock,
 * it’s never releasing it causing doSomething() method to block on waiting for the lock,
 * this will cause the system to go on deadlock and cause Denial of Service (DoS).
 */
public class MyObject_A {

    // Locks on the object's monitor
    public synchronized void doSomething() {
        // ...
    }
}

    // Hackers code
//    MyObject_A myObject = new MyObject_A();
//synchronized (myObject) {
//        while (true) {
//        // Indefinitely delay myObject
//        Thread.sleep(Integer.MAX_VALUE);
//        }
//        }
