package tutorials.multithreading.journaldev.com._1_sync_wait_notify._3_thread_safety;

class ProcessingThread implements Runnable{
    private int count;

    @Override
    public void run() {
        for(int i=1; i < 5; i++){
            processSomething(i);
            count++;
        }
    }

    /**
     *     Here are the code changes we need to do in the above program to make it thread-safe.
      */
//    //dummy object variable for synchronization
//    private Object mutex=new Object();
//    ...
//    //using synchronized block to read, increment and update count value synchronously
//    synchronized (mutex) {
//        count++;
//    }

    public int getCount() {
        return this.count;
    }

    private void processSomething(int i) {
        // processing some job
        try {
            Thread.sleep(i*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
