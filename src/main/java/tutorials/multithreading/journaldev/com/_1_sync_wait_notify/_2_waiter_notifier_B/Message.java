package tutorials.multithreading.journaldev.com._1_sync_wait_notify._2_waiter_notifier_B;

public class Message {
    private String msg;

    public Message(String str){
        this.msg=str;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String str) {
        this.msg=str;
    }

}
