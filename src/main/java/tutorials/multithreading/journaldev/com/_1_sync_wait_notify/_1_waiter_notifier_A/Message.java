package tutorials.multithreading.journaldev.com._1_sync_wait_notify._1_waiter_notifier_A;

public class Message {
    private String msg;

    public Message(String str){
        this.msg=str;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String str) {
        this.msg=str;
    }

}
