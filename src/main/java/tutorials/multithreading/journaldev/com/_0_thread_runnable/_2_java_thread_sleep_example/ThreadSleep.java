package tutorials.multithreading.journaldev.com._0_thread_runnable._2_java_thread_sleep_example;

public class ThreadSleep {

    public static void main(String[] args) throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread.sleep(2000);
        System.out.println("Sleep time in ms = "+(System.currentTimeMillis()-start));

    }

}
